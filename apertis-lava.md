# Electronica Demo Apertis image creation & automated testing

## Executive summary
The demo shows an end-to-end example of the Apertis development environment. Product-specific Linux images can be easily modified, built in the cloud then automatically tested on real hardware with fast developer feedback.

One image pipeline deploys to three embedded boards on different architectures. A web browser is installed on the image, running in kiosk mode: at image build-time the user can control which web page is shown by the demo.

The demo is targeted at embedded developers (and their managers) who need an ecosystem to built custom images for their products using reproducible and modern methods with a proven base.

## Background
### Apertis
[Apertis](https://apertis.org) is a versatile [open source infrastructure](https://www.apertis.org/architecture/platform-guide/), fit for a wide variety of electronic devices, with a history within the automotive industry. Security and modularity are two of its primary strengths. Apertis provides a feature-rich framework for add-on software and resilient upgrade capabilities. Beyond an operating system, it offers tools and cloud services to optimise development and increase reliability.

### Debos
[Debos](https://github.com/go-debos/debos) is the open-source image generation tool used by Apertis: it is desgined to make the creation of OS images simpler. Debos expects an image recipe written in YAML and runs each of the actions in the file sequentially to produce an image.

### LAVA
[LAVA](https://www.lavasoftware.org/) (Linaro Automated Validation Architecture) is an open-source continouus integration system primarily aimed at testing deployments of systems based around the Linux kernel onto physical and virtual hardware. Tests can be simple boot testing, boot-loader testing or full system level testing. Results are tracked over time and data can be exported for further analysis.

### GitLab
[GitLab](https://about.gitlab.com/) is an open-source DevOps platform which hosts code repositories, run automated builds and enables collaborative development by allowing bugs to be reported and patches to code to reviewed.

### Weston
[Weston](https://gitlab.freedesktop.org/wayland/weston) is a lightweight compositor for the Wayland protocol. It can provide a basic desktop or be used for non-desktop uses like Kiosk mode where typically a single application is run in full-screen mode.

### lava-gitlab-runner
Collabora's open-source [LAVA GitLab Runner](https://gitlab.collabora.com/lava/lava-gitlab-runner) was designed to bridge GitLab to LAVA, allowing LAVA tests to be run as part of a GitLab CI/CD pipeline. This means that new features can be tested on real hardware before they are merged: bugs can be caught before the images are deployed to customers.

## Demo information
### Image recipe modifications
The images are based on the [Reference Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes) and have been modified to run a web-browser in kiosk mode pointing at a specific URL. This URL can be simply modified at build time by making changes to a YAML file.

### Image build pipeline
Once the image has been modified, a pipeline is created and builds images for three targets; one generic amd64 image and two board-specific arm64 images.

### Hardware testing
Once the image has been built in the cloud, a LAVA job is created to run the image on the hardware and pushed to the LAVA Dispatcher. The image is run and the web browser is shown on the screens attached to the boards.

## Demo hardware requirements
- LAVA Dispatcher
	- Intel NUC
	- LCD screen

- Target hardware:
	- 1x UP Squared 6000 (amd64)
	- 1x Raspberry Pi 4 (arm64)
	- 1x ROC-RK3399-PC (arm64)
- LCD screens:
	- 3x https://www.amazon.co.uk/Touch-Screen-Monitor-Portable-Display/dp/B07Q2LBWYK


## Outstanding actions
- [ ] Find PDU for the boards
- [ ] Get Hardware from CBG and Mouser
- [ ] 5G dongle stuff
- [ ] Create booklet / some kind of handout with basic higher-level notes for CTOs / managers