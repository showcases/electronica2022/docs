### Compression

Over the past few years, different video codecs have been successfully
developed, including H.265 and VP9, to meet the needs of various
applicationsranging from video conferencing platforms like Zoom to streaming
services like YouTube and software like OBS to broadcast to different sites.

The quality of the reconstructed video using these codecs is excellent at
medium-to-low bitrates, but it degrades when operating at very low bitrates.
While these codecs leverage expert knowledge of human perception and carefully
engineered signal processing pipelines, there has been a massive interest in
replacing these handcrafted methods with machine learning approaches that learn
to encode video data.

Using open source software, Collabora has developed an efficient compression
pipeline that enables a face video broadcasting system that achieves the same
visual quality as the H.264 standard while only using one-tenth of the
bandwidth.

This demo shows the first part of the pipeline, the facial keypoints extraction,
and face reconstruction.

### Models

The demo packages two models, a deeper model that provides the best accuracy and
an optimized model (pruned and quantized) that balances runtime and accuracy.

### Directory Structure

The `compression` executable will look for the `assets` and `models` folder
before it runs the actual pipeline, so make sure both folders are in the working
directory.

### Camera Settings

Make sure the camera supports `YUV`, use the command below to list the supported
formats for your camera, adjust the device name accordingly.

```
v4l2-ctl -d /dev/video3 --list-formats-ext
```

Depending on the camera it might be necessary to adjust the format, use the
command below to adjust the format, and adjust the device name accordingly.

```
v4l2-ctl -d /dev/video3 --set-fmt-video=width=640,height=480,pixelformat=YUYV
```

### Use camera input (Default)

```
compression
```

### Use camera input with optimized model

```
compression -q
```

### Use a recorded video file

```
compression -v
```

### use a recorded video file with optimized model

```
compression -q -v
```
